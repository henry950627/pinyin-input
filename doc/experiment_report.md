## <center>拼音输入法编程作业</center>

#### <center>人工智能</center>

<div class="name">
曾祥晟&nbsp;&nbsp;2017210711
</div>

### 基本思路

根据隐马尔可夫模型，将拼音视为observation，对应的汉字视作hidden state，那么拼音输入法的问题可以视为根据观察序列求解最大概率隐藏状态序列的问题。如下图所示：

 <img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/hmm.jpg" width="410" title="HMM Graph" alt="pic1"></img>


最大概率隐藏序列的问题的最优解法是viterbi算法。通过viterbi算法进行搜索，可以求解拼音输入法的问题。具体算法过程推导如下：

设s代表隐藏状态序列，o代表观察序列， $Q_t(k)$ 代表汉字k在第t个位置出现的概率，那么：

$$
\begin{aligned}
  Q_t(k)&=\max_{s_{1:t-1}}(P(o_{1:t},s_{1:t-1},s_t=k)) \\
    &=\max_{l\in{M}}(\max_{s_{1:t-2}}(P(o_{1:t},s_{1:t-2},s_{t-1}=l,s_t=k)))\\
    &=\max_{l\in{M}}(\max_{s_{1:t-2}}(P(o_t|s_t=k)P(s_t=k|s_{t-1}=l)P(o_{1:t-1},s_{1:t-2},s_{t-1}=l)))\\
    &=\max_{l\in{M}}(P(o_t|s_t=k)P(s_t=k|s_{t-1}=l)\max_{s_{1:t-2}}(P(o_{1:t-1},s_{1:t-2},s_{t-1}=l)))\\
    &=\max_{l\in{M}}(P(o_t|s_t=k)P(s_t=k|s_{t-1}=l)Q_{t-1}(l))
\end{aligned}
$$

$$
\begin{aligned}
&上式中，P(o_t|s_t=k)代表t时刻汉字k的发射概率，\\
&假设k为多音字，可取的拼音数目为n，那么P(o_t|s_t=k)=\frac{1}{n}\\
&P(s_t=k|s_{t-1}=l)为隐藏状态转移概率。
\end{aligned}
$$

根据上述推导，只需根据语料库和汉字拼音对照表进行统计，计算出发射概率和转移概率矩阵并保存为语言模型，然后使用viterbi算法进行迭代计算即可。实际计算时，不需要遍历所有汉字的集合M，只需根据拼音在对应的汉字子集内遍历即可。假设平均每个拼音对应的汉字数为m，输入拼音的个数为d，那么算法的复杂度可粗略估计为：

$$
O(m^2d)
$$


### 实现过程

+ **词频统计**

	根据已有的新浪新闻语料库，使用`python`的`jieba`分词模块对新闻材料进行分词并统计处理，计算各词的出现的频次，或直接利用开源的词频统计数据，作为语言模型的训练数据，具体如下：
	
	<div style="margin-left: 50px">
	<img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/wordCount.png" width="" title="词频" alt="pic2"></img>
	</div>

	根据新浪语料库进行分词、统计并写入Json的操作由`src/utils/word_seg.py`中的`Segmentation`类实现；根据已有词频统计结果格式化为Json的操作由`src/utils/word_seg.py`中的`toJson`类实现。


+ **语言模型**
	
	根据词频统计的结果，计算初始概率，概率转移矩阵和汉字发射拼音的概率。以下给出公式：
	
	$$
	\begin{aligned}
	&P_{转移概率}=P(s_t=k|s_{t-1}=l)=\frac{Cnt_{l,k}}{Cnt_l}\\ \\
	&P_{初始概率}=P(s_0=k)=\frac{Cnt_{s_0=k}}{Cnt_{s_0}}\\ \\
	&P_{发射概率}=P(o_t|s_t=k)\\ \\
	&其中Cnt代表计数
	\end{aligned}
	$$
	
	具体代码实现为`src/utils/lm.py`中的`Model`类。

+ **Viterbi算法**

        def viterbi_not_iterate(self, tupleNum=2):
            """ Viterbi算法的非递归实现
                tupleNum: 字的元数n, n=1,2,3,...
            """
            for char, node in self.graph.sequence[0].iteritems():
                self.graph.sequence[0][char].max_score = \
                    self.init_prob.get(char, epsilon) + self.emission_prob[self.pinyins[0]][char]
            for kth in xrange(1, len(self.graph.sequence)):
                pinyin = self.pinyins[kth]
                for char, node in self.graph.sequence[kth].iteritems():
                    maxScore = -np.inf
                    argMaxChar = ''
                    for char_prev, node_prev in self.graph.sequence[kth-1].iteritems():
                        score = \
                            node_prev.max_score \
                            + self.emission_prob[pinyin][char]
                        actual = min(tupleNum, kth+1)
                        for i in xrange(1, actual):
                            currentChar = char if i==actual-1 else node_prev.word[-actual+1+i]
                            prev = node_prev.word[-actual+1:] if i==actual-1 else node_prev.word[-actual+1:-actual+1+i]
                            score += self.transit_prob[i-1].get(prev,{}).get(currentChar, epsilon)
                            if score > maxScore:
                            maxScore = score
                            argMaxChar = node_prev.word
                    self.graph.sequence[kth][char].max_score = maxScore
                    self.graph.sequence[kth][char].word = argMaxChar + self.graph.sequence[kth][char].word
	
	以上代码摘自自主实现的`src/hmm_graph.py`中的`Solution`类方法，实现了基于字的n元模型。
	
### 实验结果

+ **三元模型**

自定义的输入`data/input.txt`以及使用新浪新闻语料库作为训练数据，算法为基于字的三元模型，给出的输出结果`data/output.txt`分别如下所示：

<div style="padding-left: 50px">
<img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/input2.png" width="" title="词频" alt="pic2"></img>
<img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/output2.png"  width="" title="词频" alt="pic2"></img>
</div>

除第19句以外，均给出了理想的结果。第19句由于表述方式口语化，而新浪新闻的文章均以比较正式的表达书写，因此词组的组合搭配没有在语料库中出现，因此在进行对数概率计算时，使用的是默认的值计算。下面是程序在终端中输出的结果：

<div style="padding-left: 50px">
<img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/terminal.png"  width="" title="词频" alt="pic2"></img>
</div>

上图中，结果右侧的数字为对数概率值，在"xing qi yi bu xia yu"的输出中，前三的结果中对数概率相同，说明在语料模型中进行搜索时，没有相应的组合搭配出现，导致了平凡的对数概率计算结果。

+ **二元模型**

同一输入下，二元模型的结果如图所示：

<div style="padding-left: 50px">
<img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/result2.png"  width="" title="词频" alt="pic2"></img>
</div>

其中"bi ji ben dian nao"的结果并不理想，可见二元模型下对字之间建立的联系还不够强。

### 性能分析

为简便，仅存储词组的频率统计结果，程序每次运行时实时计算初始概率、发射概率和转移概率等，这一计算不列入性能分析。

在进行性能分析时，仅计算每次viterbi算法的运行时间，并在命令行中输出，展示结果如下：

<div style="padding-left: 50px">
<img src="/home/section/Documents/graduate/人工智能/拼音输入法作业/doc/img/runtime1.png"  width="" title="词频" alt="pic2"></img>
</div>

实验中计算了二元和三元模型的平均运行时间，如下表所示：

<div style="padding-left: 50px">
<table>
<tr>
<td>二元模型</td>
<td>三元模型</td>
</tr>
<tr>
<td>11.36ms</td>
<td>15.30ms</td>
</tr>
</table>
</div>

可见二元模型的运行效率较高，二者的区别在于对数转移概率相加运算的次数。

### 总结

本次实验的重点主要有三点：

1. 基础的概率模型为隐马尔可夫模型，求解的是最大概率下的隐藏状态序列问题
2. 根据语料库计算相应的概率模型，需要注意模型不同带来的计算方法的不同
3. viterbi算法的实现，采用动态规划和列表存储的方式实现

在此次实验中，我实现了基于字的二元和三元模型，并尝试了除已给新浪新闻语料库之外的结巴分词频率统计数据，其中三元模型应用于新浪新闻语料库达到了最佳的准确率。

#### 改进

尽管达到了较高的准确率，但出现错误的结果表明仍然有提升的空间。基于字的模型具有一定的局限性，从中文的语言习惯考虑，基于词的模型具有更加良好的适用性和可解释性，搜索空间也更小；语料库的使用仍然不够丰富，对于口语化的表达无法给出较精确的结果。这两点，可以作为下一步改进的地方。