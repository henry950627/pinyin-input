# -*- coding:utf-8 -*-
import json
import numpy as np
import codecs

class Model(object):
    
    """ Language Model
    """
    
    def __init__(self, wordDictDir, pyWordDictDir):
        self.wordDict = json.load(open(wordDictDir)) 
        self.pyWordDict = {}
        with open(pyWordDictDir, 'r') as f:
            for line in f:
                cs = line.rstrip('\n').rstrip('\r').decode('utf8').split(' ')
                py = cs[0]
                self.pyWordDict[py] = {}
                for c in cs[1:]:
                    self.pyWordDict[py][c] = 0

    def cal_init_prob(self):
        """ Calculate initial state probability.
        """
        dic = self.wordDict
        init_prob = {}
        for word, freq in dic.iteritems():
            startChar = word[0]
            init_prob[startChar] = init_prob.get(startChar, 0) + freq
        count = float(sum(init_prob.values()))
        for char, freq in init_prob.iteritems():
            init_prob[char] /= count
            init_prob[char] = np.log(init_prob[char])
        return init_prob

    def cal_transit_prob(self, tupleNum=2):
        """ Calculate hidden state transition probability.
            - tupleNum: 元数(二元或三元)
        """
        dic = self.wordDict
        transfer_prob = {}
        for word, freq in dic.iteritems():
            charCnt = len(word) - (tupleNum-1)
            for i in xrange(charCnt):
                if transfer_prob.has_key(word[i:i+tupleNum-1]):
                    transfer_prob[word[i:i+tupleNum-1]][word[i+tupleNum-1]] = transfer_prob[word[i:i+tupleNum-1]].get(word[i+tupleNum-1], 0) + freq
                else:
                    transfer_prob[word[i:i+tupleNum-1]] = {}
                    transfer_prob[word[i:i+tupleNum-1]][word[i+tupleNum-1]] = freq

        for word, distribute in transfer_prob.iteritems():
            cnt = float(sum(distribute.values()))
            for nextWord in distribute.keys():
                transfer_prob[word][nextWord] /= cnt
                transfer_prob[word][nextWord] = np.log(transfer_prob[word][nextWord])
        return transfer_prob

    def cal_emission_prob(self):
        """ Calculate emission probability from hidden state to observation.
        """
        dic = self.pyWordDict
        stats = {}
        for py, chars in dic.iteritems():
            for char in chars.keys():
                stats[char] = stats.get(char, 0) + 1

        for char in stats.keys():
            stats[char] = 1.0 / stats[char]

        for py, chars in dic.iteritems():
            for char in chars.keys():
                dic[py][char] = np.log(stats[char])

        return dic
    
    def run(self, tupleNum=2):

        self.init_prob = self.cal_init_prob()
        self.emission_prob = self.cal_emission_prob()
        self.transit_prob = []
        for i in xrange(2, tupleNum+1):
            self.transit_prob.append(self.cal_transit_prob(i))

if __name__ == '__main__':
    LM = Model(
        wordDictDir='word_count.json', 
        pyWordDictDir='../拼音汉字表/拼音汉字表-utf8.txt'
    )
    LM.run(3)
    with codecs.open('123.json','w','utf-8') as f:
        json.dump(LM.init_prob, f, indent=2, ensure_ascii=False)

    

