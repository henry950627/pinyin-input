# -*- coding:utf-8 -*-
import jieba
import json
import re
import time
import os
import codecs

class Segmentation(object):
    

    def __init__(self, root, srcFiles):
        self.root = root
        self.srcFiles = srcFiles
        pass

    def toFile(self, dic, fname):
        with codecs.open(fname, 'w', 'utf-8') as f:
            json.dump(dic, f, indent=2, ensure_ascii=False)

    def seg(self):

        wordDict = {}
        for src in self.srcFiles:
            print("[cut] {}".format(src))
            src = self.root + src
            with open(src, 'r') as f:
                raw = f.read()
                raw = raw.replace('\n',',').rstrip(',')
                raw = '[' + raw + ']'
                dataset = json.loads(raw)

            t0 = time.time()
            for ind, item in enumerate(dataset):
                res = jieba.cut(item['html'])#, cut_all=True)
                for w in res:
                    # Discard number, alphabet, space and single character
                    if re.match(r'[+-]?.*\d+.*', w) or re.match(r'.*\w+.*', w) or w.strip(' ') == '' or len(w)==1:
                        continue
                    wordDict[w] = wordDict.get(w,0) + 1
                if (ind+1) % 10000 == 0:
                    t1 = time.time()
                    print("[{}]items done, taking {} seconds.".format(ind+1, t1-t0))
                    t0 = t1
        return wordDict

    def run(self, dest, force=False):
        
        if not os.path.exists(dest) or force==True:
            res = self.seg()
            self.toFile(res, dest)


class toJson(object):

    def __init__(self, fname, dest):
        if not os.path.exists(dest):
            dic = {}
            with open(fname) as f:
                for line in f:
                    x = re.match(r'(.+\d+) +.+', line).group(1)
                    y = x.split('\t')
                    try:
                        dic[y[0]] = int(y[1])
                    except:
                        y = x.split(' ')
                        dic[y[0]] = int(y[1])
            with open(dest, 'w') as f:
            # with codecs.open(dest, 'w', 'utf-8') as f:
                json.dump(dic, f, indent=2, ensure_ascii=False)
            


if __name__ == '__main__':

    srcFiles = [
        # '2016-01.txt', '2016-02.txt', '2016-03.txt',
        # '2016-04.txt', '2016-05.txt', '2016-06.txt',
        # '2016-07.txt', '2016-08.txt', '2016-09.txt',
        '2016-10.txt', 
        '2016-11.txt'
    ]
    
    segSession = Segmentation(root='../../sina_news/', srcFiles=srcFiles)
    segSession.run(dest='../word_count.json')

    _2Json = toJson('../../中文语料库/分词_频数_词性@30万_未知.txt', dest='../word_count_30w.json')
