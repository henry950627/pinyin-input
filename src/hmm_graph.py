# -*- coding:utf-8 -*-
import utils.lm as lm
import numpy as np
import sys,os
import codecs
import time

epsilon = -999999

class GraphNode(object):

    def __init__(self, word):
        self.word = word
        self.max_score = epsilon

class Graph(object):

    def __init__(self, pinyins, emission_prob):
        self.sequence = []
        for py in pinyins:
            current_position = {}
            for word,emission in emission_prob[py].iteritems():
                node = GraphNode(word)
                current_position[word] = node
            self.sequence.append(current_position)

class Solution(object):

    def __init__(self, lm, pinyin=''):

        self.init_prob = lm.init_prob
        self.transit_prob = lm.transit_prob
        self.emission_prob = lm.emission_prob

        self.pinyin = pinyin
        
        
    def viterbi(self, t, k):
        node = self.graph.sequence[t][k]
        if t == 0:
            state_transfer = self.init_prob.get(k, epsilon)
            emission_prob = self.emission_prob[self.pinyins[t]][k]
            node.max_score = state_transfer + emission_prob
            return node.max_score
        pre_words = self.graph.sequence[t-1].keys()
        for l in pre_words:
            state_transfer = self.transit_prob.get(l,{}).get(k, epsilon)
            emission_prob = self.emission_prob[self.pinyins[t-1]][l]
            score = self.viterbi(t-1, l) + state_transfer + emission_prob
            if score > node.max_score:
                node.max_score = score
                node.prev_node = self.graph.sequence[t-1][l]
        self.last_node = node
        return node.max_score

    def viterbi_not_iterate(self, tupleNum=2):
        
        for char, node in self.graph.sequence[0].iteritems():
            self.graph.sequence[0][char].max_score = \
                self.init_prob.get(char, epsilon) + self.emission_prob[self.pinyins[0]][char]
        for kth in xrange(1, len(self.graph.sequence)):
            pinyin = self.pinyins[kth]
            for char, node in self.graph.sequence[kth].iteritems():
                maxScore = -np.inf
                argMaxChar = ''
                for char_prev, node_prev in self.graph.sequence[kth-1].iteritems():
                    score = \
                        node_prev.max_score \
                        + self.emission_prob[pinyin][char]
                    actual = min(tupleNum, kth+1)
                    for i in xrange(1, actual):
                        currentChar = char if i==actual-1 else node_prev.word[-actual+1+i]
                        prev = node_prev.word[-actual+1:] if i==actual-1 else node_prev.word[-actual+1:-actual+1+i]
                        score += self.transit_prob[i-1].get(prev,{}).get(currentChar, epsilon)
                    if score > maxScore:
                        maxScore = score
                        argMaxChar = node_prev.word
                self.graph.sequence[kth][char].max_score = maxScore
                self.graph.sequence[kth][char].word = argMaxChar + self.graph.sequence[kth][char].word
        
    def run_not_iterate(self, tupleNum=2):

        t0 = time.time()
        self.pinyins = self.pinyin.split(' ')
        self.graph = Graph(self.pinyins, self.emission_prob)
        self.viterbi_not_iterate(tupleNum)
        deltaT = time.time() - t0
        results = self.graph.sequence[-1].values()
        res = sorted(results, key=lambda n: n.max_score, reverse=True)

        
        printable = map(lambda x: x.word + '\t' + str(x.max_score), res)[:3]
        print('\n'.join(printable))
        print("------------consume: {}s-------------".format(deltaT))
        # print(len(self.graph.sequence))

        writable = map(lambda x: x.word, res)[0]
        return [deltaT,writable]

    
    def run(self):
        self.pinyins = self.pinyin.split(' ')
        self.graph = Graph(self.pinyins, self.emission_prob)
        res = []
        max_score = []
        for w in self.graph.sequence[-1].keys():
            self.viterbi(len(self.pinyins)-1, w)
            node = self.last_node
            res.append('')
            max_score.append(epsilon)
            while node != None:
                res[-1] = node.word + res[-1]
                max_score[-1] += node.max_score
                node = node.prev_node
        # max_score = map(lambda x: str(x), max_score)
        # printable = map(lambda x: ' '.join(x), zip(res, max_score))
        # print('\n'.join(printable))
        max_score = np.asarray(max_score)
        res = np.asarray(res)
        res = res[np.argpartition(-max_score, kth=3)][:3]
        print('\n'.join(res.tolist()))

if __name__ == '__main__':

    tn = int(sys.argv[1])
    if not os.path.exists('word_count.json'):
        import utils.word_seg as seg
        SE = seg.Segmentation(root='../sina_news/', srcFiles=[
            '2016-01.txt', '2016-02.txt', '2016-03.txt',
            '2016-04.txt', '2016-05.txt', '2016-06.txt',
            '2016-07.txt', '2016-08.txt', '2016-09.txt',
            '2016-10.txt', '2016-11.txt'
        ])
        SE.run(dest='word_count.json')

        WL = seg.toJson('../中文语料库/分词_频数_词性@30万_未知.txt', dest='word_count_30w.json')
        WL = seg.toJson('../中文语料库/分词_频数_词性@35万_结巴.txt', dest='word_count_35w.json')

    LM = lm.Model(
        wordDictDir='word_count.json', 
        # wordDictDir='word_count_30w.json', 
        # wordDictDir='word_count_35w.json', 
        pyWordDictDir='../拼音汉字表/拼音汉字表-utf8.txt'
    )
    LM.run(tn)
    
    SL = Solution(lm=LM)

    reload(sys)
    sys.setdefaultencoding('utf-8')
    runtime = []
    with open('../data/input.txt', 'r') as f:
        with open('../data/output.txt', 'w') as fw:
            for line in f:
                pinyin = line.rstrip('\n').rstrip('\r')
                SL.pinyin = pinyin
                res = SL.run_not_iterate(tn)
                fw.write("{}\n".format(res[1]))
                runtime.append(res[0])
    print(np.mean(runtime))


